# StorageRoute

Provides a simple solution to replace the "storage/" alias in the /public/ folder - useful if your hoster doesn't allow aliases.

## Installation
Installation is straightforward, setup is similar to every other Laravel Package.

#### 1. Install via Composer

Begin by pulling in the package through Composer:

```
composer require floriantraun/storageroute
```

#### 2. Define the Service Provider and alias

Next we need to pull in the alias and service providers.

**Note:** This package supports the new *auto-discovery* features of Laravel 5.5, so if you are working on a Laravel 5.5+ project, then your install is complete, you can skip to step 3.

Inside of your `config/app.php` define a new service provider

```
'providers' => [
	//  other providers

	FlorianTraun\StorageRoute\StorageRouteServiceProvider::class,
];
```

Then we want to define an alias in the same `config/app.php` file.

```
'aliases' => [
	// other aliases

	'StorageRoute' => FlorianTraun\StorageRoute\StorageRouteFacade::class,
];
```

#### 3. Publish Config File

The config file allows you to override default settings of this package to meet your specific needs.

To generate a config file type this command into your terminal:

```
php artisan vendor:publish --tag=storageroute
```

## Usage

In order to get a file URL, you need to `use StorageRoute;`. Afterwards you can use the StorageRoute functions as described in "Available Methods" below.

```
use StorageRoute;

class PageController extends Controller
{
	public function index()
	{
		echo "Route for file importantnotes.txt: " . StorageRoute::getUrl('importantnotes.txt'));
	}
}
```

### Available Methods

`$filepath` can either be a file (e.g. importantnotes.txt), or the path inside the root directory (as specified in `app/storageroute.php`) like `uploads/avatars/user01.jpg`.

* `StorageRoute::getUrl($filepath)`: Returns the URL a file.
* `StorageRoute::getContents($filepath)`: Returns the contents of a file.
* `StorageRoute::download($filepath)`: Downloads a file.

### Helper Function

Instead of calling `StorageRoute::method()`, you can use the helper function  `storageroute()`. You can use it as an alias for the class like this: `storageroute()->getUrl($filepath)`

## Contribute

I encourage you to contribute to this package to improve it and make it better. Even if you don't feel comfortable with coding or submitting a pull-request (PR), you can still support it by submitting issues with bugs or requesting new features, or simply helping discuss existing issues to give us your opinion and shape the progress of this package.

[Read the full Contribution Guide](CONTRIBUTING.md)

## Contact

I would love to hear from you.

I am always on Twitter, and it is a great way to communicate with me or follow me. [Check me out on Twitter](https://twitter.com/floriantraun).

You can also email me at flo@floriantraun.at for any other requests.
