# Contribution Guidelines

Thank you for your interest in contributing to **StorageRoute**!

## Use GitLab issues

If you experience a bug or error please submit a GitLab issue by going here: https://gitlab.com/floriantraun-laravel-packages/storageroute/issues

The issues are generally handled immediately and will receive a label as appropriate (such as "Bug", "New Feature", "Improvement", or "Out of Scope"). If you wish to fix the problem or add the feature yourself, please mention that in the issue so that a moderator can mark the issue as responsible by you and you can begin work towards submitting a PR (Pull Request).

When you submit a PR or commit, please add "Resolves \#10" or "Fixed \#22" in the commit message to link your PR/Commit to the Issue you are addressing.

## Versioning

Versioning in this package follows the [Semantic Versioning 2.0.0](http://semver.org/) standard. This is generally represented as: `MAJOR.MINOR.PATCH`.

PRs will be accepted and merged into the Dev branch. Bugs will be pushed to Master as soon as possible as a _patch_ or _incremental_ update (x.x.1), new features will be grouped together and pushed in clusters as minor upgrades (x.1.x).

Major upgrades will be noted with the community and planned in advance. These will be reserved for significant re-writes or to mark incompatibility with previous versions.

Note that the _Master_ branch is considered stable at all times and is safe to use in production. The _Dev_ branch represents the working branch and the most cutting edge features but may not be suitable for production environments.

## Thank You!

Please fill free to contribute in any way that you feel you can. Even adding issues related to bugs is helpful to the community, as is discussing new features or improvements. There are many ways like this to contribute even if you are not comfortable submitting code.

#### Contact

If you want to reach out to the Maintainer of this package, please email me at flo@floriantraun.at or follow / mention me on Twitter: https://twitter.com/floriantraun
