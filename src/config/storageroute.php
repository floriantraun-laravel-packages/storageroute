<?php

/**
 * This file is part of StorageRoute
 *
 * @license MIT
 * @package StorageRoute
 */

return [
	/*
	 * Defines the root path inside of the storage path - default is app/public/
	 */
	'root_path' => 'app/public/',
];
