<?php
namespace FlorianTraun\StorageRoute;

/**
 * This file is part of StorageRoute
 *
 * @license MIT
 * @package StorageRoute
 */

use Illuminate\Support\Facades\Facade;

class StorageRouteFacade extends Facade
{
	/**
	 * Get the registered name of the component.
	 *
	 * @return string
	 */
	protected static function getFacadeAccessor()
	{
		return 'storageroute';
	}
}
