<?php

/**
 * This file is part of StorageRoute
 *
 * @license MIT
 * @package StorageRoute
 */
 
if (! function_exists('storageroute')) {
	/**
	 * "storageroute()" helper function
	 *
	 * @return StorageRoute
	 */
	function storageroute() {
		$storageroute = app('storageroute');

		return $storageroute;
	}
}
