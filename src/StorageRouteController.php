<?php
namespace FlorianTraun\StorageRoute;

/**
 * This file is part of StorageRoute
 *
 * @license MIT
 * @package StorageRoute
 */

use Illuminate\Routing\Controller as BaseController;

class StorageRouteController extends BaseController
{
	public function index($filepath = null) {
		if (!$filepath) {
			abort(404);
		}

		return StorageRoute::getContents($filepath);
	}
}
