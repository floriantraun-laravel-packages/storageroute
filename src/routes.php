<?php

/**
 * This file is part of StorageRoute
 *
 * @license MIT
 * @package StorageRoute
 */
 
Route::name('storage')
	->get('storage/{filepath?}', 'FlorianTraun\StorageRoute\StorageRouteController@index')
	->where('filepath', '(.*)');;
