<?php
namespace FlorianTraun\StorageRoute;

/**
 * This file is part of StorageRoute
 *
 * @license MIT
 * @package StorageRoute
 */
 
class StorageRoute
{
	private function fileExists($filepath) {
		if (!file_exists(storage_path(config('storageroute.root_path', 'app/public/') . $filepath))) {
			return false;
		}

		return true;
	}

	public function getUrl($filepath) {
		if ($this->fileExists($filepath)) {
			return route('storage', ['filepath' => $filepath]);
		}
	}

	public function getContents($filepath) {
		if ($this->fileExists($filepath)) {
			return response()->file(storage_path(config('storageroute.root_path', 'app/public/') . $filepath));
		}
	}

	public function download($filepath) {
		if ($this->fileExists($filepath)) {
			return response()->download(storage_path(config('storageroute.root_path', 'app/public/') . $filepath));
		}
	}
}
