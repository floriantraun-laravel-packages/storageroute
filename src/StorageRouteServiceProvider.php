<?php
namespace FlorianTraun\StorageRoute;

/**
 * This file is part of StorageRoute
 *
 * @license MIT
 * @package StorageRoute
 */

use Illuminate\Support\ServiceProvider;

class StorageRouteServiceProvider extends ServiceProvider
{
	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;


	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->publishes([
			__DIR__.'/config/storageroute.php' => config_path('storageroute.php'),
		], 'storageroute');
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		include __DIR__ . '/routes.php';

		$this->app->singleton('storageroute', function($app) {
			return $this->app->make('FlorianTraun\StorageRoute\StorageRoute');
		});
	}
}
